package com.primitiveman.live.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.primitiveman.live.R;
import com.primitiveman.live.adapter.GridViewAdapter;
import com.primitiveman.live.item.GridViewItem;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

public class Fragment08 extends Fragment implements View.OnClickListener, LocationListener {
    private final String TAG = " Fragment08 - ";
    private ProgressDialog mProgressDialog;
    private GridView gridView;
    private ArrayList<GridViewItem> gridArr;
    private GridViewAdapter adapter;
    //private GetGridView getGridView = null;
    private String baseUrl = "";
    //private ArrayList<GridDramaItem> listViewItemArr;


    /*private int adsCnt = 0;
    private InterstitialAd interstitialAd;
    AdRequest adRequest;*/
    SharedPreferences pref;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment08, container, false);

        baseUrl = getArguments().getString("baseUrl");



        return view;
    }


    @Override
    public void onLocationChanged(Location location) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

        }
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }
}

