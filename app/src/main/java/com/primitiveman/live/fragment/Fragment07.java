package com.primitiveman.live.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.fsn.cauly.CaulyAdInfo;
import com.fsn.cauly.CaulyAdInfoBuilder;
import com.fsn.cauly.CaulyInterstitialAd;
import com.fsn.cauly.CaulyInterstitialAdListener;
import com.primitiveman.live.R;
import com.primitiveman.live.adapter.GridViewAdapter;
import com.primitiveman.live.item.GridViewItem;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

public class Fragment07 extends Fragment implements View.OnClickListener, LocationListener, CaulyInterstitialAdListener {
    private final String TAG = " Fragment07 - ";
    private ProgressDialog mProgressDialog;
    private GridView gridView;
    private ArrayList<GridViewItem> gridArr;
    private GridViewAdapter adapter;
    private String baseUrl = "";

    SharedPreferences pref;
    String clickedUrl = "";

    // CAULY
    private boolean showInterstitial = false;
    private CaulyInterstitialAd interstial;
    private CaulyAdInfo adInfo;
    private int adn = 0;
    private int adsCnt = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment07, container, false);

        baseUrl = getArguments().getString("baseUrl");

        pref= getActivity().getSharedPreferences("pref", MODE_PRIVATE); // 선언

        //gridView = (GridView)view.findViewById(R.id.gridview);
        gridArr = new ArrayList<GridViewItem>();
        gridArr.add(new GridViewItem("https://i.ytimg.com/vi/wey74oZ9tx0/hqdefault.jpg?sqp=-oaymwEZCPYBEIoBSFXyq4qpAwsIARUAAIhCGAFwAQ==&rs=AOn4CLC4qA76yWW91RbKIJTlFI0UO5ZXaQ","리치맨","https://youtu.be/wey74oZ9tx0"));
        gridArr.add(new GridViewItem("https://i.ytimg.com/vi/VVV4xeWBIxE/hqdefault.jpg?sqp=-oaymwEZCPYBEIoBSFXyq4qpAwsIARUAAIhCGAFwAQ==&rs=AOn4CLDRCtCyRH8K6pe8M2u-qH98d9UTyA","Primitive Technology","https://youtu.be/VVV4xeWBIxE"));
        gridArr.add(new GridViewItem("https://i.ytimg.com/vi/J8MLpv_utfM/hqdefault.jpg?sqp=-oaymwEZCPYBEIoBSFXyq4qpAwsIARUAAIhCGAFwAQ==&rs=AOn4CLBtSflLpPExZQHozXo6VBhf76_Glg","Primitive Technology","https://youtu.be/J8MLpv_utfM"));
        gridArr.add(new GridViewItem("https://i.ytimg.com/vi/o-2WRHY-yRU/hqdefault.jpg?sqp=-oaymwEZCPYBEIoBSFXyq4qpAwsIARUAAIhCGAFwAQ==&rs=AOn4CLCrdO_Du0kIpuNqB-yYCLPVW4AT2g","Rinnae Collins","https://youtu.be/o-2WRHY-yRU"));
        gridArr.add(new GridViewItem("https://i.ytimg.com/vi/fsq7byp8Xvs/hqdefault.jpg?sqp=-oaymwEZCPYBEIoBSFXyq4qpAwsIARUAAIhCGAFwAQ==&rs=AOn4CLAaDiM6nzjI6AgqIEyiEVfCwLSGew","시간보내기","https://youtu.be/fsq7byp8Xvs"));
        gridArr.add(new GridViewItem("https://i.ytimg.com/vi/fsq7byp8Xvs/hqdefault.jpg?sqp=-oaymwEZCPYBEIoBSFXyq4qpAwsIARUAAIhCGAFwAQ==&rs=AOn4CLAaDiM6nzjI6AgqIEyiEVfCwLSGew","Primitive Technology","https://youtu.be/J8MLpv_utfM"));


        gridView = (GridView)view.findViewById(R.id.gridview);
        adapter = new GridViewAdapter(getActivity(), gridArr, R.layout.gridviewitem);
        gridView.setAdapter(adapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                clickedUrl = gridArr.get(position).getYutubUrl();
                if(adsCnt == 0){
                    adFull();
                    adsCnt++;
                } else {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(clickedUrl));
                    startActivity(intent);
                }

            }
        });

        return view;
    }

    public void adFull(){
        adInfo = new CaulyAdInfoBuilder("Jwi7uDAD").build();
        interstial = new CaulyInterstitialAd();
        interstial.setAdInfo(adInfo);
        interstial.setInterstialAdListener(this);
        interstial.disableBackKey();
        interstial.requestInterstitialAd(getActivity());
        showInterstitial = true;
    }

    @Override
    public void onReceiveInterstitialAd(CaulyInterstitialAd caulyInterstitialAd, boolean b) {
        if (b == false) {
            Log.d("dddd", "free interstitial AD received.free interstitial AD received.");
        } else {
            Log.d("dddd", "normal interstitial AD received.");
        }

        if (showInterstitial){
            caulyInterstitialAd.show();
        } else {
            caulyInterstitialAd.cancel();
        }
    }

    @Override
    public void onFailedToReceiveInterstitialAd(CaulyInterstitialAd caulyInterstitialAd, int i, String s) {
        showInterstitial = false;
        Log.d("ffff", "free interstitial AD received.free interstitial AD received.");
        adn += 10;
        if(adn < 100){
            adFull();
        } else {
            showInterstitial = false;
            adn = 0;
        }
    }

    @Override
    public void onLocationChanged(Location location) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

        }
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onClosedInterstitialAd(CaulyInterstitialAd caulyInterstitialAd) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(clickedUrl));
        startActivity(intent);
    }

    @Override
    public void onLeaveInterstitialAd(CaulyInterstitialAd caulyInterstitialAd) {

    }
}
