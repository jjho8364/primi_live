package com.primitiveman.live.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fsn.cauly.CaulyAdInfo;
import com.fsn.cauly.CaulyAdInfoBuilder;
import com.fsn.cauly.CaulyAdView;
import com.fsn.cauly.CaulyCloseAd;
import com.fsn.cauly.CaulyCloseAdListener;
import com.fsn.cauly.CaulyInterstitialAd;
import com.fsn.cauly.CaulyInterstitialAdListener;
import com.primitiveman.live.R;
import com.primitiveman.live.fragment.Fragment01;
import com.primitiveman.live.fragment.Fragment07;
import com.primitiveman.live.fragment.Fragment08;
import com.squareup.picasso.Picasso;
import android.widget.LinearLayout.LayoutParams;

import java.util.Date;

public class MainActivity extends FragmentActivity implements View.OnClickListener, CaulyInterstitialAdListener, CaulyCloseAdListener {
    private final String TAG = " MainActivityTAG - ";

    private int mCurrentFragmentIndex;
    public final static int FRAGMENT_ONE = 0;
    public final static int FRAGMENT_TWO = 1;
    public final static int FRAGMENT_THREE = 2;
    public final static int FRAGMENT_FOUR = 3;
    public final static int FRAGMENT_FIVE = 4;
    public final static int FRAGMENT_SIX = 5;
    public final static int FRAGMENT_SEVEN = 6;
    public final static int FRAGMENT_EIGHT = 7;
    public final static int FRAGMENT_NINE = 8;
    public final static int FRAGMENT_TEN = 9;
    public final static int FRAGMENT_ELEVEN = 10;
    public final static int FRAGMENT_TWELVE = 11;
    public final static int FRAGMENT_THIRTEEN = 12;
    public final static int FRAGMENT_FOURTEEN = 13;
    public final static int FRAGMENT_HUNDRED = 99;

    private TextView tv_fragment01;
    private TextView tv_fragment02;
    private TextView tv_fragment03;
    private TextView tv_fragment04;
    private TextView tv_fragment05;
    private TextView tv_fragment06;
    private TextView tv_fragment07;
    private TextView tv_fragment08;
    private TextView tv_fragment09;
    private TextView tv_fragment10;
    private TextView tv_fragment11;
    private TextView tv_fragment12;
    private TextView tv_fragment13;
    private TextView tv_fragment100;

    private String fragment01Url = "";
    private String fragment02Url = "";
    private String fragment03Url = "";
    private String fragment04Url = "";
    private String fragment05Url = "";
    private String fragment06Url = "";
    private String fragment07Url = "";
    private String fragment08Url = "";
    private String fragment09Url = "";
    private String fragment10Url = "";
    private String fragment11Url = "";
    private String fragment12Url = "";
    private String fragment13Url = "";
    private String fragment100Url = "";

    private String nextAppUrl = "";
    private String mxPlayerUrl = "";
    private String appStatus = "99";

    private int adsCnt = 0;


    private Date curDate;
    private Date d1;
    private Date d2;

    // 카울리
    private static final String APP_CODE = "Jwi7uDAD"; // Jwi7uDAD
    private CaulyAdView adView;
    private CaulyCloseAd mCloseAd ;
    private boolean showInterstitial = false;
    private CaulyInterstitialAd interstial;
    private CaulyAdInfo adInfo;
    private int adn = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        Intent mainIntent = getIntent();

        appStatus = mainIntent.getStringExtra("appStatus");
        Log.d(TAG, "appStatus : " + appStatus);

        SharedPreferences pref= getSharedPreferences("pref", MODE_PRIVATE); // 선언
        String first = pref.getString("first",null);
        if(first != null && !appStatus.equals("3") && !appStatus.equals("4"))  appStatus = "1";

        //appStatus = "99";

        if(appStatus.equals("99")){
            tv_fragment01 = (TextView)findViewById(R.id.tv_fragment01);
            tv_fragment02 = (TextView)findViewById(R.id.tv_fragment02);
            tv_fragment03 = (TextView)findViewById(R.id.tv_fragment03);
            tv_fragment04 = (TextView)findViewById(R.id.tv_fragment04);
            tv_fragment05 = (TextView)findViewById(R.id.tv_fragment05);
            tv_fragment06 = (TextView)findViewById(R.id.tv_fragment06);
            tv_fragment07 = (TextView)findViewById(R.id.tv_fragment07);
            tv_fragment08 = (TextView)findViewById(R.id.tv_fragment08);

            tv_fragment07.setOnClickListener(this);
            tv_fragment08.setOnClickListener(this);
            fragment06Url = "";
            fragment06Url = "";

            mCurrentFragmentIndex = FRAGMENT_SEVEN;     // 첫 Fragment 를 초기화
            fragmentReplace(mCurrentFragmentIndex);

            showBanner();

            //CloseAd 초기화
            CaulyAdInfo closeAdInfo = new CaulyAdInfoBuilder(APP_CODE).build();
            mCloseAd = new CaulyCloseAd();
            //mCloseAd.setButtonText("취소", "종료");
            //mCloseAd.setDescriptionText("종료하시겠습니까?");
            mCloseAd.setAdInfo(closeAdInfo);
            mCloseAd.setCloseAdListener(this); // CaulyCloseAdListener 등록
            mCloseAd.disableBackKey();

        } else if(appStatus.equals("1")){
            setContentView(R.layout.activity_main);

            curDate = new Date();

            tv_fragment01 = (TextView)findViewById(R.id.tv_fragment01);
            tv_fragment02 = (TextView)findViewById(R.id.tv_fragment02);
            tv_fragment03 = (TextView)findViewById(R.id.tv_fragment03);
            tv_fragment04 = (TextView)findViewById(R.id.tv_fragment04);
            tv_fragment05 = (TextView)findViewById(R.id.tv_fragment05);
            tv_fragment06 = (TextView)findViewById(R.id.tv_fragment06);
            tv_fragment07 = (TextView)findViewById(R.id.tv_fragment07);
            tv_fragment08 = (TextView)findViewById(R.id.tv_fragment08);

            tv_fragment01.setOnClickListener(this);
            tv_fragment02.setOnClickListener(this);
            tv_fragment03.setOnClickListener(this);
            tv_fragment04.setOnClickListener(this);
            tv_fragment05.setOnClickListener(this);
            tv_fragment06.setOnClickListener(this);
            tv_fragment07.setOnClickListener(this);
            tv_fragment08.setOnClickListener(this);

            fragment01Url = mainIntent.getStringExtra("fragment01Url");
            fragment02Url = mainIntent.getStringExtra("fragment02Url");
            fragment03Url = mainIntent.getStringExtra("fragment03Url");
            fragment04Url = mainIntent.getStringExtra("fragment04Url");
            fragment05Url = mainIntent.getStringExtra("fragment05Url");
            fragment06Url = mainIntent.getStringExtra("fragment06Url");

            mCurrentFragmentIndex = FRAGMENT_ONE;     // 첫 Fragment 를 초기화
            fragmentReplace(mCurrentFragmentIndex);

            SharedPreferences.Editor editor = pref.edit();// editor에 put 하기
            editor.putString("first", "1"); //First라는 key값으로 id 데이터를 저장한다.
            editor.putString("adsCnt", "1"); //First라는 key값으로 id 데이터를 저장한다.
            editor.commit(); //완료한다.


            // cauly
            showBanner();

            CaulyAdInfo closeAdInfo = new CaulyAdInfoBuilder(APP_CODE).build(); //CloseAd 초기화
            mCloseAd = new CaulyCloseAd();
            //mCloseAd.setButtonText("취소", "종료");
            //mCloseAd.setDescriptionText("종료하시겠습니까?");
            mCloseAd.setAdInfo(closeAdInfo);
            mCloseAd.setCloseAdListener(this); // CaulyCloseAdListener 등록
            mCloseAd.disableBackKey();

        } else if(appStatus.equals("2")){   // maintenance
            setContentView(R.layout.maintenance);
            String maintenanceImgUrl = mainIntent.getStringExtra("maintenance");
            ImageView imgView = (ImageView)findViewById(R.id.img_maintenance);
            if(maintenanceImgUrl == null || maintenanceImgUrl.equals("")){
                imgView.setImageResource(R.drawable.noimage);
            } else {
                Picasso.with(this).load(maintenanceImgUrl).into(imgView);
            }
        } else if(appStatus.equals("3")){
            setContentView(R.layout.closed);
            String closedImgUrl = mainIntent.getStringExtra("closed");
            ImageView imgView = (ImageView)findViewById(R.id.img_closed);
            nextAppUrl = mainIntent.getStringExtra("nextAppUrl");
            if(closedImgUrl == null || closedImgUrl.equals("")){
                imgView.setImageResource(R.drawable.noimage);
            } else {
                Picasso.with(this).load(closedImgUrl).into(imgView);
            }
            Button btnClosed = (Button) findViewById(R.id.btn_closed);
            btnClosed.setOnClickListener(this);
        } else if(appStatus.equals("4")){
            setContentView(R.layout.mxplayer);
            mxPlayerUrl = mainIntent.getStringExtra("mxPlayerUrl");
            Button btnMxPlayer = (Button) findViewById(R.id.btn_mxplayer);
            btnMxPlayer.setOnClickListener(this);
        } else {

        }
    }



    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.tv_fragment01:
                offColorTv();
                tv_fragment01.setBackgroundResource(R.color.purple_300);
                mCurrentFragmentIndex = FRAGMENT_ONE;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment02:
                offColorTv();
                tv_fragment02.setBackgroundResource(R.color.purple_300);
                mCurrentFragmentIndex = FRAGMENT_TWO;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment03:
                offColorTv();
                tv_fragment03.setBackgroundResource(R.color.purple_300);
                mCurrentFragmentIndex = FRAGMENT_THREE;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment04:
                offColorTv();
                tv_fragment04.setBackgroundResource(R.color.purple_300);
                mCurrentFragmentIndex = FRAGMENT_FOUR;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment05:
                offColorTv();
                tv_fragment05.setBackgroundResource(R.color.purple_300);
                mCurrentFragmentIndex = FRAGMENT_FIVE;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment06:
                //Toast.makeText(getApplicationContext(), "준비 중인 메뉴 입니다.", Toast.LENGTH_LONG).show();
                offColorTv();
                tv_fragment06.setBackgroundResource(R.color.purple_300);
                mCurrentFragmentIndex = FRAGMENT_SIX;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment07:
                offColorTv();
                tv_fragment07.setBackgroundResource(R.color.purple_300);
                mCurrentFragmentIndex = FRAGMENT_SEVEN;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment08:
                offColorTv();
                tv_fragment08.setBackgroundResource(R.color.purple_300);
                mCurrentFragmentIndex = FRAGMENT_EIGHT;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.btn_mxplayer:
                Intent marketLaunch2 = new Intent(Intent.ACTION_VIEW);
                marketLaunch2.setData(Uri.parse(mxPlayerUrl));
                startActivity(marketLaunch2);
                break;
        }
    }

    private Fragment getFragment(int idx) {
        Fragment newFragment = null;
        Bundle args = new Bundle();
        args.putInt("adsCnt", adsCnt);

        switch (idx) {
            case FRAGMENT_ONE:
                newFragment = new Fragment01();
                args.putString("baseUrl", fragment01Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_TWO:
                newFragment = new Fragment01();
                args.putString("baseUrl", fragment02Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_THREE:
                newFragment = new Fragment01();
                args.putString("baseUrl", fragment03Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_FOUR:
                newFragment = new Fragment01();
                args.putString("baseUrl", fragment04Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_FIVE:
                newFragment = new Fragment01();
                args.putString("baseUrl", fragment05Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_SIX:
                newFragment = new Fragment01();
                args.putString("baseUrl", fragment06Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_SEVEN:
                newFragment = new Fragment07();
                args.putString("baseUrl", fragment07Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_EIGHT:
                newFragment = new Fragment08();
                args.putString("baseUrl", fragment08Url);
                newFragment.setArguments(args);
                break;
            default:
                Log.d(TAG, "Unhandle case");
                newFragment = new Fragment07();
                args.putString("baseUrl", fragment07Url);
                newFragment.setArguments(args);
                break;
        }

        return newFragment;
    }

    public void fragmentReplace(int reqNewFragmentIndex) {
        Fragment newFragment = null;
        Log.d(TAG, "fragmentReplace " + reqNewFragmentIndex);
        newFragment = getFragment(reqNewFragmentIndex);
        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction(); // replace fragment
        transaction.replace(R.id.ll_fragment, newFragment);
        transaction.commit();   // Commit the transaction
    }

    public void offColorTv(){
        tv_fragment01.setBackgroundResource(R.drawable.board_1dp);
        tv_fragment02.setBackgroundResource(R.drawable.board_1dp);
        tv_fragment03.setBackgroundResource(R.drawable.board_1dp);
        tv_fragment04.setBackgroundResource(R.drawable.board_1dp);
        tv_fragment05.setBackgroundResource(R.drawable.board_1dp);
        tv_fragment06.setBackgroundResource(R.drawable.board_1dp);
        tv_fragment07.setBackgroundResource(R.drawable.board_1dp);
        tv_fragment08.setBackgroundResource(R.drawable.board_1dp);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "main activity onResume");

        if (mCloseAd != null) mCloseAd.resume(this); // must need

        if(appStatus.equals("1") && d1 != null){
            d2 = new Date();
            long diff = d2.getTime() - d1.getTime();
            long min = diff / 1000 / 60 ;
            Log.d(TAG, "min : " + min);
            if(min >= 30){
                d1 = new Date();
                adFull();
            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            // 앱을 처음 설치하여 실행할 때, 필요한 리소스를 다운받았는지 여부.
            if (mCloseAd.isModuleLoaded()) {
                mCloseAd.show(this);
            } else {
                // 광고에 필요한 리소스를 한번만  다운받는데 실패했을 때 앱의 종료팝업 구현
                showDefaultClosePopup();
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void showDefaultClosePopup() {
        new AlertDialog.Builder(this).setTitle("").setMessage("종료 하시겠습니까?")
                .setPositiveButton("예", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setNegativeButton("아니요",null)
                .show();
    }

    // 배너 광고 삽입
    private void showBanner(){
        LinearLayout layout = (LinearLayout)findViewById(R.id.ad_ban_main);  // Root 레이아웃 참조
        CaulyAdInfo adInfo = new CaulyAdInfoBuilder(APP_CODE).effect("TopSlide").reloadInterval(1).build();
        adView = new CaulyAdView(this);
        adView.setAdInfo(adInfo);
        layout.addView(adView,0);
    }

    public void adFull(){
        adInfo = new CaulyAdInfoBuilder("Jwi7uDAD").build();
        interstial = new CaulyInterstitialAd();
        interstial.setAdInfo(adInfo);
        interstial.setInterstialAdListener(this);
        interstial.disableBackKey();
        interstial.requestInterstitialAd(this);
        showInterstitial = true;
    }

    @Override
    public void onReceiveCloseAd(CaulyCloseAd caulyCloseAd, boolean b) {

    }

    @Override
    public void onShowedCloseAd(CaulyCloseAd caulyCloseAd, boolean b) {

    }

    @Override
    public void onFailedToReceiveCloseAd(CaulyCloseAd caulyCloseAd, int i, String s) {

    }

    @Override
    public void onLeftClicked(CaulyCloseAd caulyCloseAd) {

    }

    @Override
    public void onRightClicked(CaulyCloseAd caulyCloseAd) {
        finish();
    }

    @Override
    public void onLeaveCloseAd(CaulyCloseAd caulyCloseAd) {

    }



    @Override
    public void onReceiveInterstitialAd(CaulyInterstitialAd caulyInterstitialAd, boolean b) {
        if (b == false) {
            Log.d("dddd", "free interstitial AD received.free interstitial AD received.");
        } else {
            Log.d("dddd", "normal interstitial AD received.");
        }

        if (showInterstitial){
            caulyInterstitialAd.show();
        } else {
            caulyInterstitialAd.cancel();
        }
    }

    @Override
    public void onFailedToReceiveInterstitialAd(CaulyInterstitialAd caulyInterstitialAd, int i, String s) {
        showInterstitial = false;
        Log.d("ffff", "free interstitial AD received.free interstitial AD received.");
        adn += 10;
        if(adn < 100){
            adFull();
        } else {
            showInterstitial = false;
            adn = 0;
        }
    }

    @Override
    public void onClosedInterstitialAd(CaulyInterstitialAd caulyInterstitialAd) {

    }

    @Override
    public void onLeaveInterstitialAd(CaulyInterstitialAd caulyInterstitialAd) {

    }
}
