package com.primitiveman.live.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Handler;
import android.os.Message;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.ConsoleMessage;
import android.webkit.JavascriptInterface;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.primitiveman.live.R;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.lang.reflect.InvocationTargetException;

public class LiveWebview extends Activity {
    private String TAG = " LiveWebview - ";
    private WebView webView;
    private FrameLayout customViewContainer;
    private WebChromeClient.CustomViewCallback customViewCallback;
    private View mCustomView;
    private myWebChromeClient mWebChromeClient;
    private myWebViewClient mWebViewClient;
    private String baseUrl = "";
    //private String menuId = "";
    //private String channel = "";
    private String javascriptMenu = "";
    private String javascriptId = "";
    private boolean autoCek = true;
    private String fileType = ".m3u8";
    boolean stopFlg = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_live_webview);

        baseUrl = getIntent().getStringExtra("listUrl");
        Log.d(TAG, "baseUrl : " + baseUrl);

        ImageView loadingImg = (ImageView)findViewById(R.id.gif_dog);
        GlideDrawableImageViewTarget gifImage = new GlideDrawableImageViewTarget(loadingImg);
        Glide.with(this).load(R.drawable.sleepy_dog).into(gifImage);

        customViewContainer = (FrameLayout) findViewById(R.id.customViewContainer);

        //webView = (WebView) findViewById(R.id.webView);
        webView = new WebView(getApplicationContext());
        customViewContainer.addView(webView);

        mWebViewClient = new myWebViewClient();
        webView.setWebViewClient(mWebViewClient);

        mWebChromeClient = new myWebChromeClient();
        webView.setWebChromeClient(mWebChromeClient);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setAppCacheMaxSize( 10 * 1024 * 1024 );
        webView.getSettings().setAppCachePath(getApplicationContext().getCacheDir().getAbsolutePath() );
        webView.getSettings().setAllowFileAccess( true );
        webView.getSettings().setCacheMode( WebSettings.LOAD_DEFAULT );
        //webView.addJavascriptInterface(new MyJavascriptInterface(), "Android");
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setSaveFormData(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.setBackgroundColor(0x00000000);
        webView.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);

        /*String newUA= "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2049.0 Safari/537.36\"";
        webView.getSettings().setUserAgentString(newUA);*/

        webView.getSettings().setMediaPlaybackRequiresUserGesture(false);

        webView.setWebViewClient(new WebViewClient() {

            @Override
            public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
                if(url.contains(".gif") || url.contains(".png") || url.contains(".jpg")){

                } else if(autoCek && url.contains(".m3u8")){
                    startMxPlayer(url);
                } else if(autoCek && url.contains(".mp4") && url.contains(".playercdn.net")){
                    startMxPlayer(url);
                }

                return super.shouldInterceptRequest(view, url);
            }
            /////////////////////////////////////////////////////////////////////////////////////
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.i(TAG, "Processing webview url click..." + url);
                if(url.contains("intent:")) {
                    String playUrl1 = url.split("intent:")[1];
                    String playUrl2 = playUrl1.split("#")[0];
                    Log.d(TAG, "playUrl : " + playUrl2);
                    startMxPlayer(playUrl2);
                }

                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(webView, url);
                Log.d(TAG, "onPageFinished..");
                webView.loadUrl("javascript:" + "$('.play').click()");
            }
        });

        webView.loadUrl(baseUrl);
    }

    public boolean inCustomView() {
        return (mCustomView != null);
    }

    public void hideCustomView() {
        mWebChromeClient.onHideCustomView();
    }

    @Override
    protected void onPause() {
        super.onPause();    //To change body of overridden methods use File | Settings | File Templates.
        Log.d(TAG, "onPause...");
        if (webView != null){
            try {
                Class.forName("android.webkit.WebView").getMethod("onPause", (Class[]) null) .invoke(webView, (Object[]) null);
            } catch(ClassNotFoundException cnfe) {
                cnfe.printStackTrace();
            } catch(NoSuchMethodException nsme) {
                nsme.printStackTrace();
            } catch(InvocationTargetException ite) {
                ite.printStackTrace();
            } catch (IllegalAccessException iae) {
                iae.printStackTrace();
            }
        }


        /*if(webView != null){
            destroyWebView();
        }*/
    }

    @Override
    protected void onResume() {
        super.onResume();    //To change body of overridden methods use File | Settings | File Templates.
        Log.d(TAG, "onResume...");
        if(webView != null) webView.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();    //To change body of overridden methods use File | Settings | File Templates.
        Log.d(TAG, "onStop...");
        if (inCustomView()) {
            hideCustomView();
        }
        /*if(webView != null){
            destroyWebView();
        }*/
        if (webView != null){
            try {
                Class.forName("android.webkit.WebView").getMethod("onPause", (Class[]) null) .invoke(webView, (Object[]) null);
            } catch(ClassNotFoundException cnfe) {
                cnfe.printStackTrace();
            } catch(NoSuchMethodException nsme) {
                nsme.printStackTrace();
            } catch(InvocationTargetException ite) {
                ite.printStackTrace();
            } catch (IllegalAccessException iae) {
                iae.printStackTrace();
            }
        }
        finish();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (inCustomView()) {
                hideCustomView();
                return true;
            }

            if ((mCustomView == null) && webView != null && webView.canGoBack()) {
                webView.goBack();
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    class myWebChromeClient extends WebChromeClient {
        private Bitmap mDefaultVideoPoster;
        private View mVideoProgressView;

        @Override
        public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
            String msg = consoleMessage.message()+"";
            return super.onConsoleMessage(consoleMessage);
        }

        @Override
        public void onShowCustomView(View view, int requestedOrientation, CustomViewCallback callback) {
            onShowCustomView(view, callback);    //To change body of overridden methods use File | Settings | File Templates.
        }

        @Override
        public void onShowCustomView(View view, CustomViewCallback callback) {

            // if a view already exists then immediately terminate the new one
            if (mCustomView != null) {
                callback.onCustomViewHidden();
                return;
            }
            mCustomView = view;
            webView.setVisibility(View.GONE);
            customViewContainer.setVisibility(View.VISIBLE);
            customViewContainer.addView(view);
            customViewCallback = callback;
        }

        @Override
        public View getVideoLoadingProgressView() {

            if (mVideoProgressView == null) {
                LayoutInflater inflater = LayoutInflater.from(LiveWebview.this);
                mVideoProgressView = inflater.inflate(R.layout.video_progress, null);
            }
            return mVideoProgressView;
        }

        @Override
        public void onHideCustomView() {
            super.onHideCustomView();    //To change body of overridden methods use File | Settings | File Templates.
            if (mCustomView == null)
                return;

            webView.setVisibility(View.VISIBLE);
            customViewContainer.setVisibility(View.GONE);

            // Hide the custom view.
            mCustomView.setVisibility(View.GONE);

            // Remove the custom view from its container.
            customViewContainer.removeView(mCustomView);
            customViewCallback.onCustomViewHidden();

            mCustomView = null;
        }
    }

    class myWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return super.shouldOverrideUrlLoading(view, url);    //To change body of overridden methods use File | Settings | File Templates.
        }
        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            handler.proceed(); // SSL 에러가 발생해도 계속 진행!
        }
    }

    public void startMxPlayer(String videoUrl){
        //Toast.makeText(LiveWebview.this, "2초간 종료 하지마세요.", Toast.LENGTH_SHORT).show();

        autoCek = false;
        String packageName = "";

        Intent startLink1 = getPackageManager().getLaunchIntentForPackage("com.mxtech.videoplayer.ad");
        Intent startLink2 = getPackageManager().getLaunchIntentForPackage("com.mxtech.videoplayer.pro");

        Intent intent = new Intent(Intent.ACTION_VIEW);
        Uri videoUri = Uri.parse(videoUrl);
        intent.setDataAndType(videoUri, "video/h264");
        intent.putExtra("decode_mode", (byte) 2);
        intent.putExtra("video_zoom", 0);
        intent.putExtra("fast_mode", true);
        if (startLink2 != null) {
            packageName = "com.mxtech.videoplayer.pro";
        } else {
            packageName = "com.mxtech.videoplayer.ad";
        }
        intent.setPackage(packageName);
        startActivity(intent);
        finish();
    }

    public class MyJavascriptInterface {
        @JavascriptInterface
        public void getHtml(String html){
            if(html != null && !html.equals("")){
                Document doc = Jsoup.parse(html);
                String url = doc.select("video").attr("src");
            }
        }
    }

    public void destroyWebView() {
        webView.clearHistory();
        webView.clearCache(true);
        webView.loadUrl("about:blank");
        webView.onPause();
        webView.removeAllViews();
        webView.destroyDrawingCache();
        customViewContainer.removeAllViews();
        webView.destroy();
        webView = null;
    }

    final Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            //webView.getSettings().setJavaScriptEnabled(false);
        }
    };

}
