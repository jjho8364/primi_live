package com.primitiveman.live.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.primitiveman.live.R;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class SplashActivity extends Activity {
    private String TAG = " SplashActivity - ";
    private final String baseUrl = "https://freekinglivetv.wordpress.com/manga/";
    private GetStatus getStatus = null;
    private GetMainUrl getMainUrl = null;
    private String splashInfo3 = "";

    private TextView tvSplashInfo;

    private String firstUrl = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

        tvSplashInfo = (TextView)findViewById(R.id.splash_info3);

        getMainUrl = new GetMainUrl();
        getMainUrl.execute();
        //getStatus = new GetStatus();
        //getStatus.execute();
    }

    public class GetMainUrl extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            Document doc = null;

            try {
                Log.d(TAG, "baseUrl1 : " + baseUrl);
                doc = Jsoup.connect(baseUrl).timeout(15000).get();
                String twitUrl = doc.select(".twit_addr").text();
                Log.d(TAG, "twitUrl : " + twitUrl);
                doc = Jsoup.connect(twitUrl).timeout(15000).get();
                firstUrl = doc.select(".popLinkText").text();
                Log.d(TAG, "firstUrl : " + firstUrl);

            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            getStatus = new GetStatus();
            getStatus.execute();
        }
    }

    public class GetStatus extends AsyncTask<Void, Void, Void> {

        String appStatus = "";
        String fragment01Url = "";
        String fragment02Url = "";
        String fragment03Url = "";
        String fragment04Url = "";
        String fragment05Url = "";
        String fragment06Url = "";

        String maintenanceImgUrl = "";
        String closedImgUrl = "";
        String nextAppUrl = "";
        String mxPlayerUrl = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {

            Document doc = null;

            try {
                Log.d(TAG, "baseUrl2 : " + baseUrl);
                doc = Jsoup.connect(baseUrl).timeout(15000).get();

                appStatus = doc.select(".status.real").text();
                Log.d(TAG, "appStatus : " + appStatus);

                splashInfo3 = doc.select(".splash_info3").text();

                if(appStatus.equals("1")){
                    fragment01Url = firstUrl + doc.select(".fragment01url").text();
                    fragment02Url = firstUrl + doc.select(".fragment02url").text();
                    fragment03Url = firstUrl + doc.select(".fragment03url").text();
                    fragment04Url = firstUrl + doc.select(".fragment04url").text();
                    fragment05Url = firstUrl + doc.select(".fragment05url").text();
                    fragment06Url = firstUrl + doc.select(".fragment06url").text();

                    mxPlayerUrl = doc.select(".mxplayer").text();
                    Intent startLink1 = getPackageManager().getLaunchIntentForPackage("com.mxtech.videoplayer.ad");
                    Intent startLink2 = getPackageManager().getLaunchIntentForPackage("com.mxtech.videoplayer.pro");
                    if(startLink1 == null && startLink2 == null){
                        appStatus = "4";
                        mxPlayerUrl = doc.select(".mxplayer").text();
                    }

                } else if(appStatus.equals("2")){
                    maintenanceImgUrl = doc.select(".maintenance").text();
                    fragment01Url = firstUrl + doc.select(".fragment01url").text();
                    fragment02Url = firstUrl + doc.select(".fragment02url").text();
                    fragment03Url = firstUrl + doc.select(".fragment03url").text();
                    fragment04Url = firstUrl + doc.select(".fragment04url").text();
                    fragment05Url = firstUrl + doc.select(".fragment05url").text();
                    fragment06Url = firstUrl + doc.select(".fragment06url").text();

                } else if(appStatus.equals("3")){
                    closedImgUrl = doc.select(".closed").text();
                    nextAppUrl = doc.select(".newappurl").text();
                } else if(appStatus.equals("9")){
                    closedImgUrl = doc.select(".closed").text();
                    nextAppUrl = doc.select(".mid.site").text();
                } else {
                    maintenanceImgUrl = doc.select(".maintenance").text();
                }
            } catch(Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            tvSplashInfo.setText(splashInfo3);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(getApplication(), MainActivity.class);

                    intent.putExtra("appStatus", appStatus);

                    if(appStatus.equals("1")){
                        intent.putExtra("fragment01Url", fragment01Url);
                        intent.putExtra("fragment02Url", fragment02Url);
                        intent.putExtra("fragment03Url", fragment03Url);
                        intent.putExtra("fragment04Url", fragment04Url);
                        intent.putExtra("fragment05Url", fragment05Url);
                        intent.putExtra("fragment06Url", fragment06Url);
                    } else if(appStatus.equals("2")){
                        intent.putExtra("maintenance", maintenanceImgUrl);
                        intent.putExtra("fragment01Url", fragment01Url);
                        intent.putExtra("fragment02Url", fragment02Url);
                        intent.putExtra("fragment03Url", fragment03Url);
                        intent.putExtra("fragment04Url", fragment04Url);
                        intent.putExtra("fragment05Url", fragment05Url);
                        intent.putExtra("fragment06Url", fragment06Url);
                    } else if(appStatus.equals("3")){
                        intent.putExtra("closed", closedImgUrl);
                        intent.putExtra("nextAppUrl", nextAppUrl);
                    } else if(appStatus.equals("4")){
                        intent.putExtra("mxPlayerUrl", mxPlayerUrl);
                    } else if(appStatus.equals("9")){
                        intent.putExtra("closed", closedImgUrl);
                        intent.putExtra("nextAppUrl", nextAppUrl);
                    } else {
                        intent.putExtra("maintenance", maintenanceImgUrl);
                    }
                    finish();
                    startActivity(intent);
                }
            }, 3000);
        }
    }
}
